
plugins {
    scala
    id("com.github.johnrengelman.shadow") version "7.1.1"
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

dependencies {

    implementation("com.typesafe.scala-logging:scala-logging_2.11:3.9.4")
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("com.amazonaws:AWSGlueETL:3.0.0")
    implementation("org.scala-lang:scala-library:2.11.12")
    implementation("com.amazon.redshift:redshift-jdbc41:1.2.10.1009")
    testImplementation("org.scalatest:scalatest_2.11:3.2.12")
    testImplementation("junit:junit:4.13.2")
    implementation ("com.lihaoyi:requests_2.12:0.7.0")
    implementation("com.auth0:auth0:1.35.0")
}

tasks {
    shadowJar {
        archiveBaseName.set("job_scripts")
        archiveClassifier.set("")
        archiveVersion.set("")
        isZip64 = true
        from(sourceSets["main"].output)
    }
}

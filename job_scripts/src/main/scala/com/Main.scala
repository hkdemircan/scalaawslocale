package com

import com.amazonaws.services.glue.GlueContext
import com.amazonaws.services.glue.util.{GlueArgParser, Job, JsonOptions}
import com.typesafe.config.ConfigFactory
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.JavaConverters._

object Main {

  def main(sysArgs: Array[String]): Unit = {
    val database = "the dbname which is created by crawler" // aws glue -> data catalog -> databases
    val tableName = "table name which is created by crawler, " // aws glue -> data catalog -> databases -> tables

    val catalogConnectionName = "catalogConnectionName"
    val glueContext: GlueContext = new GlueContext(getOrCreateSparkSession(isAWS))
    val args = GlueArgParser.getResolvedOptions(sysArgs, Seq("TempDir", "JOB_NAME", "DB_NAME").toArray)
    Job.init(args("JOB_NAME"), glueContext, args.asJava)
    val databaseName = args("DB_NAME") // redshift db name

    //input
    val datasource0 = glueContext.getCatalogSource(
      database = database,
      tableName = tableName,
      redshiftTmpDir = "",
      transformationContext = "datasource0")
      .getDynamicFrame()

    //transform
    val appliedFields = datasource0.applyMapping(
      Seq(
        ("name", "string", "name", "string"),
        ("age", "int", "age", "int"),
        ("address.state", "string", "state", "string"),
        ("address.zip", "int", "zip", "int")
      ),
      caseSensitive = false,
      transformationContext = "stgsdkmetricsrequest"
    )

    //drop + load
    val tablePreQuery = "begin;TRUNCATE TABLE {redshiftTableName}; end;"

    // it will create the redshift table automatically
    glueContext.getJDBCSink(
      catalogConnection = catalogConnectionName,
      options = JsonOptions(
        s"""{
                    "dbtable": "redshiftTableName",
                    "database": "$databaseName",
                    "preactions": "$tablePreQuery"
                }""".stripMargin),
      redshiftTmpDir = args("TempDir"),
      transformationContext = "datasink1"
    ).writeDynamicFrame(appliedFields)

    Job.commit()
  }

  def getOrCreateSparkSession(isAWS: Boolean): SparkContext = {
    val conf = new SparkConf().set("spark.app.name", "applicationName")
    if (!isAWS) {
      conf.set("spark.master", "local")
    }
    val spark: SparkContext = SparkContext.getOrCreate(conf)
    spark.setLogLevel("ERROR")
    spark
  }

  private def isAWS: Boolean = {
    ConfigFactory.load().getConfig("etl").getBoolean("isAWS")
  }
}

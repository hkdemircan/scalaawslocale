allprojects {
    repositories {
        maven("https://aws-glue-etl-artifacts.s3.amazonaws.com/release/") {
            name = "aws-glue-etl-artifacts"
        }
        maven("https://s3.amazonaws.com/redshift-maven-repository/release") {
            name = "aws-redshift-artifacts"
        }
        mavenCentral()
    }
}